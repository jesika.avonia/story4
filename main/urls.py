from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('intro/', views.intro, name='intro'),
    path('profile/', views.profile, name='profile'),
    path('experiences/', views.experiences, name='experiences'),
    path('hobby/', views.hobby, name='hobby'),
]
