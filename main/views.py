from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')


def profile(request):
    return render(request, 'main/profile.html')

def intro(request):
    return render(request, 'main/intro.html')

def experiences(request):
    return render(request, 'main/experiences.html')

def hobby(request):
    return render(request, 'main/hobby.html')
